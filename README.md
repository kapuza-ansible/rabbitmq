# RabbitMQ cluster install on Ubuntu
## rabbit_action
* **install_settings** - install and base setup
* **install** - base install
* **settings** - make settings (NODENAME, cookie, managment consoile)
* **user** - user (add_user, set_user_tags, set_permissions)
* **slave** - join slave to master

## ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash
allow_world_readable_tmpfiles=True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```
## Install python (ubuntu)
For use ansible, you need install python2.7 and python-apt on remote server.
```bash
ansible rabbit -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt"
```

## Download ansible role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml
---
- src: git+https://gitlab.com/kapuza-ansible/rabbitmq.git
  name: rabbitmq
EOF

echo "roles/rabbitmq" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## Use role
```bash
cat << 'EOF' > install_rabbitmq.yml
---
- name: Install RabbitMQ
  hosts:
    rabbit
  become: yes
  become_user: root
  tasks:
    - name: Rabbit
      include_role:
        name: rabbitmq
      vars:
        rabbit_action: '{{ item }}'
      with_items:
        - 'install_settings'
        - 'user'

- name: Install RabbitMQ cluster
  hosts:
    rabbit_slaves
  become: yes
  become_user: root
  tasks:
    - name: Rabbit
      include_role:
        name: rabbitmq
      vars:
        rabbit_action: '{{ item }}'
        rabbit_master: 'rabbit01'
      with_items:
        - 'slave'
EOF

ansible-playbook  install_rabbitmq.yml
```
